const login = function (vm, user) {
  return new Promise((resolve, reject) => {
    if (!vm) {
      reject('missing vue instance')
      return
    }
    vm.$http.post('/api/v1/login', user).then(resp => {
      resolve(resp)
    }).catch(err => {
      reject(err)
    })
  })
}

const getProfile = function (vm) {
  return new Promise((resolve, reject) => {
    if (!vm) {
      reject('missing vue instance')
      return
    }
    vm.$http.get('/api/v1/profile').then(resp => {
      resolve(resp)
    }).catch(err => {
      reject(err)
    })
  })
}

const resetPassword = function (vm, data) {
  return new Promise((resolve, reject) => {
    if (!vm) {
      reject('missing vue instance')
      return
    }
    vm.$http.put('/api/v1/user/reset-password', data).then(resp => {
      resolve(resp)
    }).catch(err => {
      reject(err)
    })
  })
}
export default {
  login,
  getProfile,
  resetPassword
}
