import axios from 'axios'

console.log('booting axios...')

const DEBUG_MODE = true
window.baseURL = DEBUG_MODE ? 'http://localhost:8082/' : 'http://hotel.bolow.co'

var http = axios.create({
  baseURL: window.baseURL,
  timeout: 15 * 1000,
  headers: {
    'Content-Type': 'application/json'
  }
})
http.interceptors.request.use(config => {
  let auth = window.localStorage.getItem('auth')
  if (auth !== null) {
    auth = JSON.parse(auth)
    config.headers.Authorization = auth.token
  }
  return config
}, err => {
  console.log('axiso error', err)
})

export default async ({ Vue }) => {
  // Vue.prototype.$axios = axios
  Vue.prototype.$http = http
}
