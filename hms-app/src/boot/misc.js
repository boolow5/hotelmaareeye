import Vue from 'vue'
import KnobControl from 'vue-knob-control'
// import { Vue2Dragula } from 'vue2-dragula'
import { date } from './../mixins'

console.log('booting misc...')

Vue.use(KnobControl)
console.log('installed KnobControl')

// Vue.use(Vue2Dragula, {
//   logging: {
//     service: true // to only log methods in service (DragulaService)
//   }
// })

Vue.mixin(date)

window.clone = (obj) => {
  if (typeof obj === 'object') {
    return JSON.parse(JSON.stringify(obj))
  }
  return obj
}

export default ({ Vue }) => {
  // add plugin
}
