import Vue from 'vue'
import Vuex from 'vuex'

import rooms from './modules/rooms'
import user from './modules/user'
import guests from './modules/guests'
import payments from './modules/payments'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      rooms,
      user,
      guests,
      payments
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
