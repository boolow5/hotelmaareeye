const state = {
  rooms: [
    { id: 1, room_number: 1, is_vacant: true, is_available: true, guest: {}, checked_in_date: null, checked_out_date: null, amenities: { 'AC': false, 'TV': true, 'Private Bathroom': true, 'WiFi': true } },
    { id: 2, room_number: 2, is_vacant: true, is_available: true, guest: { id: 2, name: 'Mahdi Ahmed', phone: '+254741620827' }, checked_in_date: 1566397453, checked_out_date: 1566401628, amenities: { 'AC': true, 'TV': true, 'Private Bathroom': true, 'WiFi': true } },
    { id: 3, room_number: 3, is_vacant: true, is_available: true, guest: {}, checked_in_date: null, checked_out_date: null, amenities: { 'AC': true, 'TV': true, 'Private Bathroom': true, 'WiFi': true } },
    { id: 4, room_number: 4, is_vacant: false, is_available: true, guest: {}, checked_in_date: null, checked_out_date: null, amenities: { 'AC': true, 'TV': true, 'Private Bathroom': true, 'WiFi': true } },
    { id: 5, room_number: 5, is_vacant: true, is_available: true, guest: {}, checked_in_date: null, checked_out_date: null, amenities: { 'AC': true, 'TV': true, 'Private Bathroom': true, 'WiFi': true } },
    { id: 6, room_number: 6, is_vacant: false, is_available: true, guest: {}, checked_in_date: null, checked_out_date: null, amenities: { 'AC': true, 'TV': true, 'Private Bathroom': true, 'WiFi': true } },
    { id: 7, room_number: 7, is_vacant: true, is_available: false, guest: {}, checked_in_date: null, checked_out_date: null, amenities: { 'AC': true, 'TV': true, 'Private Bathroom': true, 'WiFi': true } },
    { id: 8, room_number: 8, is_vacant: true, is_available: true, guest: {}, checked_in_date: null, checked_out_date: null, amenities: { 'AC': true, 'TV': true, 'Private Bathroom': true, 'WiFi': true } },
    { id: 9, room_number: 9, is_vacant: true, is_available: true, guest: {}, checked_in_date: null, checked_out_date: null, amenities: { 'AC': true, 'TV': true, 'Private Bathroom': true, 'WiFi': true } },
    { id: 10, room_number: 10, is_vacant: true, is_available: true, guest: {}, checked_in_date: null, checked_out_date: null, amenities: { 'AC': true, 'TV': true, 'Private Bathroom': true, 'WiFi': true } },
    { id: 11, room_number: 11, is_vacant: true, is_available: false, guest: {}, checked_in_date: null, checked_out_date: null, amenities: { 'AC': true, 'TV': true, 'Private Bathroom': true, 'WiFi': true } }
  ]
}

const getters = {
  rooms: (state) => {
    if (state.rooms.length === 0) {
      return JSON.parse(localStorage.getItem('rooms') || '{}')
    }
    return state.rooms
  },
  get_room_by_number: (state) => num => {
    let room = state.rooms.filter(item => item && item.room_number === parseInt(num))
    console.log('get_room_by_number', [num, window.clone(room)])
    if (room && room.length > 0) {
      return room[0]
    }
    return null
  }
}

const mutations = {
  SET_ROOMS (state, payload) {
    state.rooms = payload || []
    localStorage.setItem('rooms', JSON.stringify(state.rooms))
  },
  UPDATE_ROOM (state, payload) {
    for (var i = 0; i < state.rooms.length; i++) {
      if (state.rooms[i] && state.rooms[i].room_number === payload.room_number) {
        console.info('FOUND ROOM', window.clone(state.rooms[i]))
        state.rooms[i] = payload
      }
    }
  }
}

const actions = {
  UPDATE_ROOM ({ commit, state }, payload) {
    console.log('UPDATE_ROOM', payload)
    if (!payload && payload.room_number) {
      console.error('CANNOT UPDATE INVALID ROOM')
      return
    }
    commit('UPDATE_ROOM', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
