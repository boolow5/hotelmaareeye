const state = {
  guests: [
    { id: 1, name: 'Mahdi Bolow', phone: '0741620827' },
    { id: 2, name: 'Mahdi Ahmed', phone: '+254741620827' },
    { id: 3, name: 'Mahad Bala', phone: '00254741620827' }
  ]
}

const getters = {
  guests: (state) => state.guests,
  get_guest_by_number: (state) => num => {
    let guest = state.guests.filter(item => item && item.id === parseInt(num))
    if (guest && guest.length > 0) {
      return guest[0]
    }
    return null
  }
}

const mutations = {
  SET_GUESTS (state, payload) {
    state.guests = payload || []
  },
  UPDATE_GUEST (state, payload) {
    for (var i = 0; i < state.guests.length; i++) {
      if (state.guests[i] && state.guests[i].id === payload.id) {
        console.info('FOUND GUEST', window.clone(state.guests[i]))
        state.guests[i] = payload
      }
    }
  }
}

const actions = {
  UPDATE_GUEST ({ commit, state }, payload) {
    console.log('UPDATE_GUEST', payload)
    if (!payload && payload.id) {
      console.error('CANNOT UPDATE INVALID GUEST')
      return
    }
    commit('UPDATE_GUEST', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
