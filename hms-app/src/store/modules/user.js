const state = {
  token: '',
  profile: {},
  level: 0
}

const getters = {
  token: (state) => state.token,
  profile: (state) => state.profile,
  level: (state) => state.level,
  isAuthenticated: (state) => state.token && state.token.length > 50
}

const mutations = {
  SET_TOKEN (state, payload) {
    state.token = payload
    localStorage.setItem('auth', JSON.stringify(state))
  },
  SET_LEVEL (state, payload) {
    state.level = payload
    localStorage.setItem('auth', JSON.stringify(state))
  },
  SET_PROFILE (state, payload) {
    state.profile = payload
    localStorage.setItem('auth', JSON.stringify(state))
  }
}

const actions = {
  LOGIN_USER ({ commit }, payload) {
    commit('SET_TOKEN', payload.token)
    commit('SET_PROFILE', payload.profile)
    commit('SET_LEVEL', payload.level || 0)
  },
  LOGOUT_USER ({ commit }, payload) {
    commit('SET_TOKEN', '')
    commit('SET_PROFILE', {})
    commit('SET_LEVEL', 0)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
