const state = {
  transactions: [
    { id: 1, name: 'Mahdi Bolow', phone: '0741620827' },
    { id: 2, name: 'Mahdi Ahmed', phone: '+254741620827' },
    { id: 3, name: 'Mahad Bala', phone: '00254741620827' }
  ],
  default_price_per_day: 500,
  default_price_currency: 'Ksh'
}

const getters = {
  transactions: (state) => state.transactions,
  get_transaction_by_number: (state) => num => {
    let transaction = state.transactions.filter(item => item && item.id === parseInt(num))
    if (transaction && transaction.length > 0) {
      return transaction[0]
    }
    return null
  },
  default_price_per_day (state) {
    if (!state.default_price_per_day) {
      return JSON.parse(localStorage.getItem('default_price_per_day') || '500')
    }
    return state.default_price_per_day
  },
  default_price_currency (state) {
    if (!state.default_price_currency) {
      return JSON.parse(localStorage.getItem('default_price_currency') || 'Ksh')
    }
    return state.default_price_currency
  }
}

const mutations = {
  SET_TRANSACTIONS (state, payload) {
    state.transactions = payload || []
  },
  UPDATE_TRANSACTION (state, payload) {
    for (var i = 0; i < state.transactions.length; i++) {
      if (state.transactions[i] && state.transactions[i].id === payload.id) {
        console.info('FOUND TRANSACTION', window.clone(state.transactions[i]))
        state.transactions[i] = payload
      }
    }
  },
  UPDATE_DEFAULT_PRICE_PER_DAY (state, payload) {
    state.default_price_per_day = parseFloat(payload)
  },
  UPDATE_DEFAULT_CURRENCY (state, payload) {
    state.default_price_currency = parseFloat(payload)
  }
}

const actions = {
  UPDATE_TRANSACTION ({ commit, state }, payload) {
    console.log('UPDATE_TRANSACTION', payload)
    if (!payload && payload.id) {
      console.error('CANNOT UPDATE INVALID TRANSACTION')
      return
    }
    commit('UPDATE_TRANSACTION', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
