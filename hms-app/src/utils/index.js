const SECONDS = {
  year: 31536000,
  month: 2592000,
  week: 604800,
  day: 86400,
  hour: 3600,
  minute: 60,
  second: 1
}

const SECONDS_KEYS = ['year', 'month', 'week', 'day', 'hour', 'minute', 'second']

export const daysPast = function (unixdate, secondUnix, callerName) {
  let unix = unixdate
  console.log('daysPast', { unixdate, secondUnix, callerName })
  if (typeof unixdate === 'string') {
    unix = parseInt(unixdate)
  }
  if (typeof unix !== 'number' || Number.isNaN(unix)) {
    return unixdate
  }
  const output = {}
  if (!secondUnix) {
    secondUnix = new Date().getTime() / 1000
  }
  unix = Math.round(secondUnix) - unix
  SECONDS_KEYS.forEach(key => {
    console.log('key: ' + key, { output: Math.floor(unix / SECONDS[key]), unix })

    output[key] = Math.floor(unix / SECONDS[key])
    unix -= output[key] * SECONDS[key]
  })
  return output
}
