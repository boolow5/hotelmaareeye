const unixdiff = function (unixdate) {
  let unix = unixdate
  console.log('unixdiff', unixdate)
  if (typeof unixdate === 'string') {
    unix = parseInt(unixdate)
  }
  if (typeof unix !== 'number' || Number.isNaN(unix)) {
    return unixdate
  }
  unix = Math.round(new Date().getTime() / 1000) - unix
  if (unix > 60 * 60 * 60 * 24 * 30) {
    return `${Math.round(unix / (60 * 60 * 60 * 24 * 30))}Y`
  } else if (unix > 60 * 60 * 60 * 24) {
    return `${Math.round(unix / (60 * 60 * 60 * 24))}M`
  } else if (unix > 60 * 60 * 60) {
    return `${Math.round(unix / (60 * 60 * 60))}d`
  } else if (unix > 60 * 60) {
    return `${Math.round(unix / (60 * 60))}h`
  } else if (unix > 60) {
    return `${Math.round(unix / 60)}m`
  }

  return `${Math.round(unix)}s`
}

export default {
  filters: {
    unixdiff
  }
}
