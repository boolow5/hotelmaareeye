
const routes = [
  {
    path: '/login',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Login.vue') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/guests', component: () => import('pages/Users.vue') },
      { path: '/dashboard', component: () => import('pages/Dashboard.vue') },
      { path: '/payments', component: () => import('pages/Payments.vue') },
      { path: '/rooms', component: () => import('pages/Rooms.vue') },
      { path: '/profile', component: () => import('pages/Profile.vue') }
    ],
    beforeEnter: (to, from, next) => {
      console.log('Layout.beforeEnter', { to, from, next })
      let authStr = localStorage.getItem('auth')
      let auth = {}
      if (authStr) {
        auth = JSON.parse(authStr)
        if (auth && typeof auth.token === 'string' && auth.token.length < 100) {
          next({ path: '/login', query: { next: to.path } })
          return
        }
      } else {
        next({ path: '/login' })
        return
      }
      next()
    }
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
