package main

import (
	"fmt"
	"os"

	"bitbucket.org/boolow5/HotelMaareeye/hms-api/config"
	"bitbucket.org/boolow5/HotelMaareeye/hms-api/db"
	"bitbucket.org/boolow5/HotelMaareeye/hms-api/logger"
	"bitbucket.org/boolow5/HotelMaareeye/hms-api/models"
	"bitbucket.org/boolow5/HotelMaareeye/hms-api/notifications"
	"bitbucket.org/boolow5/HotelMaareeye/hms-api/router"
)

func main() {
	logger.Init(logger.DebugLevel)
	conf := config.Get()
	models.Version = conf.Version
	fmt.Printf("Starting %s...\n", os.Args[0])
	db.Init()
	err := models.Init()
	if err != nil {
		panic(err)
	}
	router := router.Init()
	go notifications.Start(conf.SSEPort)
	notifications.InitMailGun(conf.MailGun)
	router.Run(conf.Port)
}
