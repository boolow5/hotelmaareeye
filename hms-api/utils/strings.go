package utils

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// URLQuery represents a query from url
type URLQuery struct {
	Key   string
	Value string
}

// IsValidEmail checks if an email is valid
func IsValidEmail(email string) bool {
	pattern := "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
	// pattern := "([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+.[a-zA-Z0-9_-]+)"
	r, _ := regexp.Compile(pattern)
	return r.Match([]byte(email))
}

// IsValidPhone checks if a phone number string is actually real number
func IsValidPhone(phone string) bool {
	isClean, num := CleanPhoneNumber(phone)
	length := len(fmt.Sprintf("%s", num))
	if isClean && (length == 9 || length == 10 || length == 12) {
		return true
	}
	return false
}

// CleanPhoneNumber cleans phone number from unnecessary chars
func CleanPhoneNumber(phone string) (isClean bool, clean string) {
	clean = strings.Replace(
		strings.Replace(
			strings.Replace(
				phone,
				" ", "", -1),
			"-", "", -1),
		"+", "", 1)
	num, err := strconv.ParseInt(clean, 10, 64)
	isClean = (num > 0 && err == nil)
	return isClean, clean
}

// StringIn checks if str is found in arr or not
func StringIn(str string, arr []string, ignoreCase bool) bool {
	for i := 0; i < len(arr); i++ {
		val1 := str
		val2 := arr[i]
		if ignoreCase {
			val1 = strings.ToLower(str)
			val2 = strings.ToLower(arr[i])
		}
		if val1 == val2 {
			return true
		}
	}
	return false
}

// GetURLQueries parses url and gets key/value pairs from the query parameters
func GetURLQueries(url string) []URLQuery {
	var data []URLQuery
	urlParts := strings.Split(url, "?")
	if len(urlParts) < 2 {
		return data
	}
	querySection := urlParts[1]
	parts := strings.Split(querySection, "&")
	for i := 0; i < len(parts); i++ {
		keyVal := strings.Split(parts[i], "=")
		if len(keyVal) > 1 {
			data = append(data, URLQuery{Key: keyVal[0], Value: keyVal[1]})
		} else if len(keyVal) == 1 {
			data = append(data, URLQuery{Key: keyVal[0], Value: "true"})
		}
	}
	return data
}
