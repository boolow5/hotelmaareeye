package models

import (
	"fmt"
	"strings"
)

const (
	// ValidationError represents validation error
	ValidationError = iota
	// AuthenticationError represents authentication error
	AuthenticationError
	// HashingError represents hashing error
	HashingError
	// NotFoundError represents if item was not found in the database
	NotFoundError
	// InvalidLocationAddress represents when location has no formatted address
	InvalidLocationAddress
	// InvalidLocationCoordinates represents when location has zero coordinates
	InvalidLocationCoordinates
)

var (
	// ErrorMessages holds messages that represent error codes above
	ErrorMessages = []string{
		"validation error",
		"authentication error",
		"hashing error",
		"not found",
		"invalid location address",
		"invalid location coordinates",
	}
)

// ModelError is custom error object
type ModelError struct {
	Code     int    `json:"code" bson:"code"`
	Message  string `json:"msg" bson:"msg"`
	MoreInfo string `json:"more" bson:"more"`
}

// Error satisfies error interface
func (me ModelError) Error() string {
	return fmt.Sprintf("%d %s: %s", me.Code, me.Message, me.MoreInfo)
}

// NewError creates ModelError
func NewError(code int, moreInfo string) ModelError {
	return ModelError{Code: code, Message: ErrorMessages[code], MoreInfo: moreInfo}
}

// JoinErrors conactinates all errors and joins with comas
func JoinErrors(errs []error) error {
	errMessages := []string{}
	for i := 0; i < len(errs); i++ {
		if errs[i] == nil {
			continue
		}
		errMessages = append(errMessages, errs[i].Error())
	}
	return fmt.Errorf("%s", strings.Join(errMessages, ", "))
}
