package models

import (
	"fmt"
	"os"

	"bitbucket.org/boolow5/HotelMaareeye/hms-api/db"
	"bitbucket.org/boolow5/HotelMaareeye/hms-api/logger"
	"gopkg.in/mgo.v2/bson"
)

// Init initializes models
func Init() error {
	count, _ := db.Count(UserCollection, &bson.M{})
	pass := os.Getenv("IWEYDI_INITIAL_PASS")
	firstName := os.Getenv("IWEYDI_INITIAL_USER_FIRSTNAME")
	middleName := os.Getenv("IWEYDI_INITIAL_USER_MIDDLENAME")
	lastName := os.Getenv("IWEYDI_INITIAL_USER_LASTNAME")
	email := os.Getenv("IWEYDI_INITIAL_EMAIL")
	phone := os.Getenv("IWEYDI_INITIAL_PHONE")
	if count == 0 && pass == "" {
		panic("FATAL ERROR: initial pass env is required to run for the first time")
	}
	if count == 0 && email == "" {
		panic("FATAL ERROR: initial email env is required to run for the first time")
	}
	if firstName == "" {
		firstName = "Mahdi"
	}
	if middleName == "" {
		middleName = "Ahmed"
	}
	if lastName == "" {
		lastName = "Bolow"
	}
	if phone == "" {
		phone = "0741620827"
	}
	if count == 0 {
		id := bson.NewObjectId()
		user := User{
			ID:    id,
			Email: email,
			Phone: phone,
			Profile: Profile{
				UserID:     id,
				FirstName:  firstName,
				LastName:   lastName,
				MiddleName: middleName,
			},
		}
		user.SetPassword(pass)
		if err := db.CreateStrong(UserCollection, &user); err != nil {
			panic(fmt.Errorf("failed to create initial user: %s", err.Error()))
		}
	}

	return nil
}

func createIndexes() {
	logger.Debug("*************** Adding indexes ***************")
	// err := db.AddUniqueIndex(UserCollection, "email", "phone")
	// if err != nil {
	// 	logger.Errorf("Failed to add unique fields 'email,phone' to '%s' collection. Error: %v", UserCollection, err)
	// }
	// err = db.Add2DIndex(DistrictCollection, "$2d:center")
	// if err != nil {
	// 	logger.Errorf("Failed to add 2d index 'center' to '%s' collection. Error: %v", DistrictCollection, err)
	// }
}
